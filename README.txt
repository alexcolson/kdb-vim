This repository contains a copy of the work of http://code.kx.com/wsvn/code/contrib/simon/vim/
It is just a convience to be able to use it with git / as a bundle.
All credits to the original author Simon Garland. 


I use it with my vimrc script from here : https://bitbucket.org/alexcolson/vimrc

If you want to install it as a seperately here are the steps below.

First of all it requires the pathogen plugin which can be obtained here : https://github.com/tpope/vim-pathogen
(along with install instructions)

Then go under you .vim folder, and type the following commands:
mkdir -p bundle
git clone git@bitbucket.org:alexcolson/kdb-vim.git


And that's all. 


